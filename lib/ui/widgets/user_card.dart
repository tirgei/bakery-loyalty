import 'package:bakery_loyalty/data/models/user.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class UserCard extends StatefulWidget {
  final User user;
  final GestureTapCallback onTap;

  UserCard({Key? key, required this.user, required this.onTap}) : super(key: key);

  @override
  State createState() => _UserCardState();
}

class _UserCardState extends State<UserCard> {

  @override
  Widget build(BuildContext context) {
    final user = widget.user;

    return Card(
      child: ListTile(
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Name: ${user.firstName} ${user.lastName}", style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 18.0,
                letterSpacing: 1.0,
                color: Colors.grey[800]
            )),
            Text("Phone: ${user.phone}",
                style: TextStyle(
                    fontSize: 16,
                    color: Colors.grey[500],
                    fontWeight: FontWeight.bold
                )),
            Text("Points: ${user.points}",
                style: TextStyle(
                    fontSize: 16,
                    color: Colors.grey[500],
                    fontWeight: FontWeight.bold
                ))
          ],
        ),
        onTap: () => widget.onTap(),
      ),
    );
  }
}