import 'package:bakery_loyalty/data/network/database.dart';
import 'package:bakery_loyalty/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AddUserPage extends StatefulWidget {
  static const String route = "/addUser";

  const AddUserPage({Key? key}) : super(key: key);

  @override
  _AddUserPageState createState() => _AddUserPageState();
}

class _AddUserPageState extends State<AddUserPage> {
  final database = new Database();
  TextEditingController _firstnametextFieldController =
      new TextEditingController();
  TextEditingController _lastnametextFieldController =
      new TextEditingController();
  TextEditingController _phonetextFieldController = new TextEditingController();
  bool loading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        elevation: 0,
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text("Add user",
        style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 20.0,
            letterSpacing: 1.0,
            color: Colors.white
        )),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
              child: Text(
                "Add New Customer",
                textAlign: TextAlign.center,
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
              child: TextFormField(
                controller: _firstnametextFieldController,
                decoration: const InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: 'FirstName',
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
              child: TextFormField(
                controller: _lastnametextFieldController,
                decoration: const InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: 'Lastname',
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
              child: TextFormField(
                controller: _phonetextFieldController,
                decoration: const InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: 'PhoneNumber',
                ),
              ),
            ),
            new Container(
              width: MediaQuery.of(context).size.width,
              margin: const EdgeInsets.only(top: 20.0),
              alignment: Alignment.center,
              child: new Row(
                children: <Widget>[
                  loading
                      ? Container(
                          width: MediaQuery.of(context).size.width,
                          alignment: Alignment.center,
                          child: CircularProgressIndicator(),
                      )
                      : new Expanded(
                          child: Container(
                            padding: EdgeInsets.symmetric(horizontal: 10),
                            child: ElevatedButton(
                              style: ButtonStyle(
                                  backgroundColor: MaterialStateProperty.all<Color>(
                                      Theme.of(context).primaryColor),
                                  shape: MaterialStateProperty.all<
                                          RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0),
                              ))),
                              onPressed: () {
                                setState(() {
                                  loading = true;
                                });
                                addUser(context);
                              },
                              child: Text("Submit".toUpperCase()),
                            ),
                          ),
                        ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  void addUser(BuildContext context) async {
    final success = await database.saveUser({
      "firstName": _firstnametextFieldController.text,
      "lastName": _lastnametextFieldController.text,
      "phone": _phonetextFieldController.text,
      "points": 0,
    });

    setState(() {
      loading = false;
    });

    if (success) {
      Utils.toast("User added");
      Navigator.pop(context);
    } else {
      Utils.toast("Unable to add user");
    }
  }
}
