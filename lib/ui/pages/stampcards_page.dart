import 'package:bakery_loyalty/data/models/user.dart';
import 'package:bakery_loyalty/data/network/database.dart';
import 'package:bakery_loyalty/utils/utils.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

class StampCardsPage extends StatefulWidget {
  static const String route = "/stampcards";

  const StampCardsPage({Key? key}) : super(key: key);

  @override
  _StampCardsPageState createState() => _StampCardsPageState();
}

class _StampCardsPageState extends State<StampCardsPage> {
  final database = new Database();
  var userId;

  @override
  Widget build(BuildContext context) {
    final args =
        ModalRoute.of(context)!.settings.arguments as StampCardsArguments;
    userId = args.userId;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        elevation: 0,
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text("User Stamps",
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20.0,
                letterSpacing: 1.0,
                color: Colors.white)),
      ),
      body: StreamBuilder<DocumentSnapshot>(
        stream: database.getUser(userId),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else {
            final user = User.fromSnapshot(snapshot.data!.data());
            SchedulerBinding.instance!.addPostFrameCallback((_) {
              if (user.points >= 10) {
                showRewardDialog();
              }
            });

            return Container(
              color: Colors.grey[200],
              child: GridView.builder(
                padding: EdgeInsets.all(10),
                itemCount: user.points,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 3,
                    crossAxisSpacing: 4.0,
                    mainAxisSpacing: 4.0),
                itemBuilder: (BuildContext context, int index) {
                  return Image(
                    image: AssetImage("assets/stamp.png"),
                  );
                },
              ),
            );
          }
        },
      ),
    );
  }

  void showRewardDialog() {
    showDialog<String>(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        title: const Text('Reward!!'),
        content: const Text('You have won yourself a reward!!'),
        actions: <Widget>[
          TextButton(
            onPressed: () async {
              Navigator.pop(context);
              var success = await database.redeemReward(userId);
              if (success) {
                Utils.toast("Reward redeemed!");
              } else {
                Utils.toast("Unable to redeem reward");
              }
            },
            child: const Text('Claim'),
          ),
        ],
      ),
    );
  }
}

class StampCardsArguments {
  final String userId;

  StampCardsArguments(this.userId);
}
