import 'dart:math';

import 'package:bakery_loyalty/data/models/user.dart';
import 'package:bakery_loyalty/data/network/database.dart';
import 'package:bakery_loyalty/ui/pages/stampcards_page.dart';
import 'package:bakery_loyalty/utils/utils.dart';
import 'package:flutter/material.dart';

class ProcessPaymentPage extends StatefulWidget {
  static const String route = "/processPayment";

  const ProcessPaymentPage({Key? key}) : super(key: key);

  @override
  _ProcessPaymentPageState createState() => _ProcessPaymentPageState();
}

class _ProcessPaymentPageState extends State<ProcessPaymentPage> {
  final database = new Database();
  var user;
  var amount;

  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context)!.settings.arguments as ProcessPaymentArguments;
    validatePayment(context, args.user, args.amount);

    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                CircularProgressIndicator(),
                SizedBox(height: 20),
                Text("Processing payment...")
              ],
            ),
          ),
        ],
      ),
    );
  }

  void validatePayment(BuildContext context, User user, int amount) async {
    if  (amount < 10) {
      Utils.toast("Not eligible for points");
      Navigator.pop(context);
      return;
    }

    var success = await database.processUserPoints(user);
    if (success) {
      Navigator.popAndPushNamed(
        context,
        StampCardsPage.route,
        arguments: StampCardsArguments(user.id)
      );
    } else {
      Navigator.pop(context);
    }
  }

}

class ProcessPaymentArguments {
  final User user;
  final int amount;

  ProcessPaymentArguments(this.user, this.amount);
}
