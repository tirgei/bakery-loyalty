import 'package:bakery_loyalty/data/models/user.dart';
import 'package:bakery_loyalty/data/network/database.dart';
import 'package:bakery_loyalty/ui/pages/add_user_page.dart';
import 'package:bakery_loyalty/ui/pages/process_payment_page.dart';
import 'package:bakery_loyalty/ui/widgets/user_card.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DashboardPage extends StatefulWidget {
  static const String route = "/dashboard";

  DashboardPage({Key? key}) : super(key: key);

  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  final database = new Database();
  bool loading = false;
  var points = 10;

  TextEditingController _amounttextFieldController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        elevation: 0,
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(
            "Customers",
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 20.0,
              letterSpacing: 1.0,
              color: Colors.white
            )),
      ),
      body: StreamBuilder<QuerySnapshot>(
        stream: database.getUsers(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else if(snapshot.data != null && snapshot.data!.docs.length < 1) {
            return Center(
              child: Text("No users found"),
            );
          } else {
            final users = snapshot.data?.docs.map((doc) => User.fromSnapshot(doc.data())) ?? [];
            return Container(
              color: Colors.grey[200],
              height: MediaQuery.of(context).size.height,
              child: ListView(
                shrinkWrap: true,
                padding: EdgeInsets.all(10),
                children: users.map((user) {
                  return UserCard(
                    user: user,
                    onTap: () => paymentDialog(user),
                  );
                }).toList(),
              ),
            );
          }
        },
      ),

      floatingActionButton: FloatingActionButton(
        backgroundColor: Theme.of(context).primaryColor,
        focusColor: Theme.of(context).primaryColor,
        tooltip: 'add user',
        onPressed: () {
          Navigator.pushNamed(
            context,
            AddUserPage.route,
          );

        },
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  void paymentDialog(User user) {
    showDialog<String>(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        title: const Text('Enter purchase amount'),
        content: TextField(
          keyboardType: TextInputType.number,
          controller: _amounttextFieldController,
          decoration: InputDecoration(hintText: "€"),
        ),
        actions: <Widget>[
          TextButton(
            onPressed: () => Navigator.pop(context),
            child: const Text('Cancel'),
          ),
          TextButton(
            onPressed: () {
              var amount = int.parse(_amounttextFieldController.text);
              _amounttextFieldController.clear();
              Navigator.popAndPushNamed(
                context,
                ProcessPaymentPage.route,
                arguments: ProcessPaymentArguments(user, amount)
              );
            },
            child: const Text('OK'),
          ),
        ],
      ),
    );
  }
}

