class User {
  String id;
  String firstName;
  String lastName;
  String phone;
  int points;

  User({
    required this.id,
    required this.firstName,
    required this.lastName,
    required this.phone,
    required this.points
  });

  factory User.fromSnapshot(dynamic data) => User(
    id: data["id"],
    firstName: data["firstName"],
    lastName: data["lastName"],
    phone: data["phone"],
    points: data["points"],
  );

}