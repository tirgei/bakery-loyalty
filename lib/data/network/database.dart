import 'package:bakery_loyalty/data/models/user.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class Database {
  final firestore = FirebaseFirestore.instance;

  /// Save New User details
  Future<bool> saveUser(Map<String, dynamic> payload) async {
    final usersRef = firestore.collection("users");
    try {
      var id = usersRef.doc().id;
      payload["id"] = id;
      await firestore.collection("users")
        .doc(id)
        .set(payload)
        .then((value) {
          print("User added");
        });
      return true;
    } catch (e) {
      print("Error adding user: $e");
      return false;
    }
  }

  /// Fetch list of saved users
  Stream<QuerySnapshot> getUsers() {
    try {
      CollectionReference usersRef = firestore.collection("users");
      return usersRef.snapshots();
    } catch (e) {
      print("Error fetching users: $e");
      return Stream.empty();
    }
  }

  /// Fetch list of saved users
  Stream<DocumentSnapshot> getUser(String userId) {
    try {
      DocumentReference usersRef = firestore.collection("users").doc(userId);
      return usersRef.snapshots();
    } catch (e) {
      print("Error fetching user: $e");
      return Stream.empty();
    }
  }

  /// Increment user points
  Future<bool> processUserPoints(User user) async {
    try {
      print("Incrementing ${user.id} points from ${user.points}");
      await firestore.collection("users")
          .doc(user.id)
          .update({"points": user.points+1});
      return true;
    } catch (e) {
      print("Error updating user points: $e");
      return false;
    }

  }

  /// Increment user points
  Future<bool> redeemReward(String userId) async {
    final usersRef = firestore.collection("users");
    try {
      var userDoc = await usersRef.doc(userId).get();
      var user = User.fromSnapshot(userDoc.data());

      await firestore.collection("users")
          .doc(user.id)
          .update({"points": user.points-10});
      return true;
    } catch (e) {
      print("Error updating user points: $e");
      return false;
    }

  }


}