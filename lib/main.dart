import 'package:bakery_loyalty/ui/pages/add_user_page.dart';
import 'package:bakery_loyalty/ui/pages/dashboard_page.dart';
import 'package:bakery_loyalty/ui/pages/process_payment_page.dart';
import 'package:bakery_loyalty/ui/pages/stampcards_page.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';


void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  //rest of the code
  runApp(MyApp());
}
class MyApp extends StatefulWidget {
  _AppState createState() => _AppState();
}
class _AppState extends State<MyApp> {
  // Set default `_initialized` and `_error` state to false
  bool _initialized = false;
  bool _error = false;

  // Define an async function to initialize FlutterFire
  void initializeFlutterFire() async {
    try {
      // Wait for Firebase to initialize and set `_initialized` state to true
    //  await Firebase.initializeApp();
      setState(() {
        _initialized = true;
      });
    } catch(e) {
      // Set `_error` state to true if Firebase initialization fails
      setState(() {
        _error = true;
      });
    }
  }
  @override
  void initState() {
    initializeFlutterFire();
    super.initState();
  }
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    if(_error) {
      print('NOT CONNECTED');
    }
    // Show a loader until FlutterFire is initialized
    if (!_initialized) {
      print('LOADING');
    }
    return MaterialApp(
      title: 'Bakery Loyalty',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(fontFamily: 'Nunito', primaryColor: Colors.deepOrange),
      initialRoute: DashboardPage.route,
      routes: {
        DashboardPage.route: (context) => DashboardPage(),
        AddUserPage.route: (context) => AddUserPage(),
        ProcessPaymentPage.route: (context) => ProcessPaymentPage(),
        StampCardsPage.route: (context) => StampCardsPage()
      },
    );
  }
}


