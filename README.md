# Bakery Loyalty

A demo project for the reward points system

## Use Case

Amani is bakery a owner.
He has loyalty card sytem that is composed of 10 squares.
For each buy that exceed 10 euros, he stamp one square.
When the client card 10 squares are all stamped, the client is reward.

Amani want to replace is the loyalty paper card to a mobile app.
- Break the application into many different modules.
- How you will manage the security?
- Implement a mobile app only the interface to submit the purchase tracking for each client.

You can use any material that you think useful.

You can share your submission by sharing your git url.
The submission needs to contain a commented code and a readme file to explain how to build and deploy the app.
Or you may deploy it into the available store and invite us as a tester.

## Setup App
To set up the app:
- Clone this project
- Head over to Firebase and create a new project. In the project add the respective app i.e iOS or Android that you 
want to build and generate the relevant google-services file. Download this and place in the appropriate app folder.
- Navigate to the root folder of the project and run `flutter pub get`. This should install all required dependencies.
- You should now be able to run the app 🚀

### Screenshots
|                 |                  |          
| --------------- | ---------------- |
| ![Scheme](screenshots/one.png) | ![Scheme](screenshots/two.png) | 
| ![Scheme](screenshots/three.png) | ![Scheme](screenshots/four.png) |

